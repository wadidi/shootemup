﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleEnemyBullet : Bullet {

    // Use this for initialization
    void Start()
    {
        Speed = new Vector2(-5, 0);
    }

    // Update is called once per frame
    void Update () {
        UpdatePosition();
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        // If the other is an enemy
        if (other.gameObject.tag == "Player")
        {
            other.GetComponent<BaseAvatar>().TakeDamage(this.Damage);
            Destroy(this.gameObject);
        }
    }
}
