﻿using UnityEngine;

public class LevelDescription
{
    public string Name;
    public EnemyDescription Enemies;
}
