﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAvatar : BaseAvatar {

	// Use this for initialization
	void Start () {
        Health = MaxHealth;
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    // Destroy object if it goes out of the screen
    void OnBecameInvisible()
    {
        Destroy(gameObject);
    }

    public override void TakeDamage(float damage)
    {
        Health -= damage;
        if (Health <= 0)
        {
            Die();
        }
    }
}
