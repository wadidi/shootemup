﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletFactory : MonoBehaviour {

    [SerializeField]
    public GameObject enemyBulletPrefab;

    [SerializeField]
    public GameObject playerBulletPrefab;

    private BulletFactory Instance;

    public List<GameObject> playerBulletsEnsemble; // Contains a list of bullets that disappeared and are available to be shot

    public Bullet GetBullet(BulletType type)
    {
        GameObject bulletObject = null;
        switch(type)
        {
            case BulletType.PLAYER_BULLET:
                if(playerBulletsEnsemble.Count == 0) // Instantiate only if no player bullet is available in the storage
                {
                    bulletObject = GameObject.Instantiate(playerBulletPrefab);
                }
                else
                {
                    bulletObject = playerBulletsEnsemble[0];
                    bulletObject.SetActive(true);
                    playerBulletsEnsemble.RemoveAt(0);
                }
                break;
            case BulletType.ENEMY_BULLET:
                bulletObject = GameObject.Instantiate(enemyBulletPrefab);
                break;
        }

        Bullet bullet = bulletObject.GetComponent<Bullet>();

        return bullet;
    }

	// Use this for initialization
	void Start () {
        playerBulletsEnsemble = new List<GameObject>();
	}
	
	// Update is called once per frame
	void Update () {
	}
}
