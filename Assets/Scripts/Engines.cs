﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Engines : MonoBehaviour {

    private Vector2 speed;
    private Vector2 position;
    
    public Vector2 Speed
    {
        get
        {
            return this.speed;
        }
        set
        {
            this.speed = value;
        }
    }

    public Vector2 Position
    {
        get
        {
            return this.position;
        }
        set
        {
            this.position = value;
        }
    }

    // Use this for initialization
    void Start () {
        this.Position = this.transform.position;
	}
	
	// Update is called once per frame
	void Update () {
        transform.Translate(this.Speed * this.GetComponent<BaseAvatar>().MaxSpeed * Time.deltaTime);
    }
}
