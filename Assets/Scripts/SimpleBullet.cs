﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleBullet : Bullet {

    private BulletFactory bulletFactory;

	// Use this for initialization
	void Start () {
        Init();
        Speed = new Vector2(10, 0);
        bulletFactory = GameObject.Find("GameManager").GetComponent<BulletFactory>();
	}
	
	// Update is called once per frame
	void Update () {
        UpdatePosition();
	}

    void OnTriggerEnter2D (Collider2D other)
    {
        // If the other is an enemy
        if (other.gameObject.tag == "Enemy")
        {
            other.GetComponent<BaseAvatar>().TakeDamage(this.Damage);
            bulletFactory.playerBulletsEnsemble.Add(this.gameObject);
            this.gameObject.SetActive(false);
        }
    }
}
