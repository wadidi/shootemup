﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAvatar : BaseAvatar {

    private float lastReloadTime;
    public float reloadLifeCooldown;
    public float reloadLifeCooldownValue;

    // Health event
    public delegate void HealthEvent(float health);
    public static event HealthEvent OnHealthChange;

    // Use this for initialization
    void Start () {
        Health = MaxHealth;
        lastReloadTime = Time.time;
    }
	
	// Update is called once per frame
	void Update () {

        // Reload life
        if (Time.time - lastReloadTime > reloadLifeCooldown)
        {
            Health += reloadLifeCooldownValue;
            lastReloadTime = Time.time;
            OnHealthChange(Health);
        }

        // Maintain player in camera scope

        if (transform.position.x < -9.0F)
        {
            Vector2 newPosition = new Vector2(-9.0F, transform.position.y);
            transform.position = newPosition;
        }

        if (transform.position.x > 9.0F)
        {
            Vector2 newPosition = new Vector2(9.0F, transform.position.y);
            transform.position = newPosition;
        }

        if (transform.position.y > 5.0F)
        {
            Vector2 newPosition = new Vector2(transform.position.x, 5.0F);
            transform.position = newPosition;
        }

        if (transform.position.y < -5.0F)
        {
            Vector2 newPosition = new Vector2(transform.position.x, -5.0F);
            transform.position = newPosition;
        }
    }

    public override void TakeDamage(float damage)
    {
        Health -= damage;
        OnHealthChange(Health);
        if (Health <= 0)
        {
            Die();
        }
    }
}
