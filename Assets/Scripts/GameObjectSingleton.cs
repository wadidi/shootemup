﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameObjectSingleton : MonoBehaviour
{
    [SerializeField]
    private Vector2 initialPlayerPosition;

    [SerializeField]
    public GameObject playerPrefab;

    [SerializeField]
    public GameObject enemyPrefab;

    [SerializeField]
    private float cooldownEnemyInstantiation;

    [SerializeField]
    protected Slider shootEnergySlider;

    [SerializeField]
    protected Slider healthSlider;

    protected GameObject playerInstance;
    private float initialShootEnergy;
    private float initialHealth;

    public Vector2 InitialPlayerPosition
    {
        get
        {
            return this.initialPlayerPosition;
        }
        set
        {
            this.initialPlayerPosition = value;
        }
    }

    public static GameObjectSingleton instance = null;              //Static instance of GameManager which allows it to be accessed by any other script.
    private int level = 1;
    public int initialNumberOfEnemies = 10;
    private int roundNumberOfEnemies;
    private int numberOfInstantiatedEnemies = 0;

    private Text roundText;

    //public LevelDescription Data;
    //public Level CurrentLevel;

    //Awake is always called before any Start functions
    void Awake()
    {
        //Check if instance already exists
        if (instance == null)

            //if not, set instance to this
            instance = this;

        //If instance already exists and it's not this:
        else if (instance != this)

            //Then destroy this. This enforces our singleton pattern, meaning there can only ever be one instance of a GameManager.
            Destroy(gameObject);

        //Sets this to not be destroyed when reloading scene
        DontDestroyOnLoad(gameObject);

        //  Set the number of enemies for the round to its initial state
        roundNumberOfEnemies = initialNumberOfEnemies;

        //Call the InitGame function to initialize the first level 
        InitGame();

        roundText = GameObject.Find("Round").GetComponent<Text>();
    }

    //Initializes the game for each level.
    void InitGame()
    {
        //  Instantiate the player
        if (level == 1)
        {
            playerInstance = (GameObject)Instantiate(playerPrefab);
            playerInstance.transform.position = InitialPlayerPosition;
            initialShootEnergy = playerInstance.GetComponent<BulletGun>().ShootEnergy;
            initialHealth = playerInstance.GetComponent<BaseAvatar>().MaxHealth;
        }

        InvokeRepeating("InstantiateEnemy", 0, cooldownEnemyInstantiation);

        //LevelDescription FirstLevelDescription = new LevelDescription();
        //TextAsset asset = Resources.Load("FirstLevelDescription");
        //FirstLevelDescription = XmlHelpers.DeserializeFromXML<LevelDescription>("../FirstLevelDescription.xml");
        //CurrentLevel = new Level();
        //CurrentLevel.Initialize(FirstLevelDescription);
            
    }

    int Fibonacci(int num)
    {
        if (num <= 1) return 1;
        return Fibonacci(num - 1) + Fibonacci(num - 2);
    }

    void InstantiateEnemy()
    {
        numberOfInstantiatedEnemies++;

        //  Instantiate enemies
        GameObject enemyInstance = (GameObject)Instantiate(enemyPrefab);

        //  Random position
        enemyInstance.transform.position = new Vector2(9, Random.Range(-4.6F, 4.6F));
    }

    void UpdateShootEnergySlider(float energy)
    {
        shootEnergySlider.value = energy/initialShootEnergy;
    }

    void UpdateHealthSlider(float health)
    {
        healthSlider.value = health / initialHealth;
    }

    private void OnEnable()
    {
        //  Add event listeners
        BulletGun.OnShootEnergyChange += UpdateShootEnergySlider;
        PlayerAvatar.OnHealthChange += UpdateHealthSlider;
    }

    private void OnDisable()
    {
        BulletGun.OnShootEnergyChange -= UpdateShootEnergySlider;
        PlayerAvatar.OnHealthChange += UpdateHealthSlider;
    }


    //Update is called every frame.
    void Update()
    {
        if (numberOfInstantiatedEnemies == roundNumberOfEnemies)
        {
            CancelInvoke();

            //  If there is no more enemy, next level called
            if (GameObject.FindGameObjectsWithTag("Enemy").Length == 0)
            {
                level++;
                roundText.text = "Round" + level;
                roundNumberOfEnemies = Fibonacci (level+1); // The number of enemies per round follows a Fibonacci series
                numberOfInstantiatedEnemies = 0;
                InitGame();
            }
        }

        if (Input.GetKeyDown("escape"))
        {

            // Destroy all game objects
            foreach (GameObject o in Object.FindObjectsOfType<GameObject>())
            {
                Destroy(o);
            }
            SceneManager.LoadScene("mainMenu");
        }

    }
}
