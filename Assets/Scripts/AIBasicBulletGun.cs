﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIBasicBulletGun : MonoBehaviour {

    private BulletFactory bulletFactory;

    [SerializeField]
    private float cooldown;

    private Vector2 offset = new Vector2(-0.3F, 0.0F); // Align the bullet gun with the bottom of the spaceship

    public float CoolDown
    {
        get
        {
            return this.cooldown;
        }
        set
        {
            this.cooldown = value;
        }
    }

    // Use this for initialization
    void Start () {
        InvokeRepeating("Fire", 0, CoolDown);
        bulletFactory = GameObject.Find("GameManager").GetComponent<BulletFactory>();
    }

    // Instantiate a bullet if possible
    public void Fire()
    {
        Bullet simpleBullet = bulletFactory.GetBullet(BulletType.ENEMY_BULLET);
        simpleBullet.gameObject.transform.position = (Vector2)this.transform.position + offset;
    }

    // Update is called once per frame
    void Update () {
		
	}
}
