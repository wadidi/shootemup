﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputController : MonoBehaviour {

    private Engines engines;
    private float cooldown;

    // Energy event
    public delegate void FireEvent();
    public static event FireEvent OnFire;

    // Use this for initialization
    void Start () {
        engines = this.GetComponent<Engines>();
        cooldown = GetComponent<BulletGun>().CoolDown;
    }
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKey("space") || Input.GetButton("XboxA")) // XboxA for the Xbox controller
        {
            OnFire();
        }

        // Move the object
        engines.Speed = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));
	}

    void UpdateShootEnergySlider(float energy)
    {
        if (energy == 0)
        {
            CancelInvoke(); // Stop reloading normally if there is no more shoot energy
        }
    }

    private void OnEnable()
    {
        //  Add event listeners
        BulletGun.OnShootEnergyChange += UpdateShootEnergySlider;
    }

    private void OnDisable()
    {
        BulletGun.OnShootEnergyChange -= UpdateShootEnergySlider;
    }
}
