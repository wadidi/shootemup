﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour {

    public void LoadScene()
    {
        SceneManager.LoadScene("primaryScene");
    }

    public void QuitGame ()
    {
        Application.Quit();
    }
}
