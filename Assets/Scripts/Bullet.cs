﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Bullet : MonoBehaviour {

    [SerializeField]
    private float damage;
    private Vector2 speed;
    private Vector2 position;

    public float Damage
    {
        get
        {
            return this.damage;
        }
        set
        {
            this.damage = value;
        }
    }

    public Vector2 Speed
    {
        get
        {
            return this.speed;
        }
        set
        {
            this.speed = value;
        }
    }

    public Vector2 Position
    {
        get
        {
            return this.position;
        }
        set
        {
            this.position = value;
        }
    }

    protected virtual void Init()
    {
        Speed = new Vector2(0, 0);
        Position = gameObject.transform.position;
    }

    protected virtual void UpdatePosition()
    {
        transform.Translate(this.Speed * Time.deltaTime);
    }

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {

	}

    // Destroy object if it goes out of the screen
    void OnBecameInvisible()
    {
    }
}
