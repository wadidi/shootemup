﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level {
    public LevelDescription definition;
    public void Initialize(LevelDescription levelDescription)
    {
        definition = levelDescription;
    }
}
