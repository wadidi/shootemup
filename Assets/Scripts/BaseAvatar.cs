﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public abstract class BaseAvatar : MonoBehaviour {

    [SerializeField]
    private float maxSpeed;
    private float health;

    [SerializeField]
    private float maxHealth;

    public float MaxHealth
    {
        get
        {
            return this.maxHealth;
        }
        set
        {
            this.maxHealth = value;
        }
    }


    public float Health
    {
        get
        {
            return this.health;
        }
        set
        {
            this.health = value;
        }
    }

    public float MaxSpeed
    {
        get
        {
            return this.maxSpeed;
        }

        private set
        {
            this.maxSpeed = value;
        }
    }

    // Diminish the health

    public virtual void TakeDamage (float damage)
    {

    }

    //  Kill the character
    public void Die()
    {
        Destroy(this.gameObject);
    }


    // Use this for initialization
    void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
