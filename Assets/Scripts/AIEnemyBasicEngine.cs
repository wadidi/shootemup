﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIEnemyBasicEngine : MonoBehaviour {

    private Engines engines;
    private BaseAvatar avatar;

	// Use this for initialization
	void Start () {
        avatar = this.GetComponent<BaseAvatar>();
        engines = this.GetComponent<Engines>();
        engines.Speed = new Vector2(-Random.Range(0.2F, avatar.MaxSpeed), 0); // Go Left. Uniform linear motion
    }
	
	// Update is called once per frame
	void Update () {
	}
}
