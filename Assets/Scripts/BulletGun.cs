﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletGun : MonoBehaviour {

    [SerializeField]
    public GameObject playerBulletPrefab;

    [SerializeField]
    private float cooldown;

    [SerializeField]
    private float reloadCooldown;

    [SerializeField]
    private float reloadValue;

    private BulletFactory bulletFactory;

    private Vector2 offset = new Vector2(1.15F, -0.6F); // Align the bullet gun with the bottom of the spaceship

    // Energy event
    public delegate void EnergyEvent(float energy);
    public static event EnergyEvent OnShootEnergyChange;

    public bool canFire;

    public float CoolDown
    {
        get
        {
            return this.cooldown;
        }
        set
        {
            this.cooldown = value;
        }
    }

    public float ReloadCoolDown
    {
        get
        {
            return this.reloadCooldown;
        }
        set
        {
            this.reloadCooldown = value;
        }
    }

    public float ReloadValue
    {
        get
        {
            return this.reloadValue;
        }
        set
        {
            this.reloadValue = value;
        }
    }

    [SerializeField]
    private float shootEnergy;

    public float ShootEnergy
    {
        get
        {
            return this.shootEnergy;
        }
        set
        {
            this.shootEnergy = value;
        }
    }

    private float initialShootEnergy;
    private float lastShootTime;
    private float lastReloadTime;

    //  Fire one bullet
    public void Fire ()
    {
        Bullet playerBulletInstance = bulletFactory.GetBullet(BulletType.PLAYER_BULLET);
        playerBulletInstance.transform.position = (Vector2)this.transform.position + offset;   //  Match the instance with the bullet of the splite
    }

    // Instantiate a bullet if possible
    public void FireManager ()
    {
        if (canFire == true)
        {
            if (lastShootTime == 0.0F)
            {
                lastShootTime = Time.time;
            }
            if (Time.time - lastShootTime > CoolDown)
            {
                Fire();
                ShootEnergy--;
                OnShootEnergyChange(ShootEnergy);
                if (ShootEnergy <= 0)
                {
                    canFire = false;
                }
                lastShootTime = Time.time;
            }
        }
    }

    private void OnEnable()
    {
        //  Add event listeners
        InputController.OnFire += FireManager;
    }

    private void OnDisable()
    {
        InputController.OnFire -= FireManager;
    }



    public void Reload ()
    {
        if (ShootEnergy < initialShootEnergy)
        {
            if (!canFire)
            {
                ShootEnergy += (ReloadValue * 0.75F); // 25% Slower
            }
            else
            {
                ShootEnergy += ReloadValue;
            }
            OnShootEnergyChange(ShootEnergy);
        }
        if (ShootEnergy >= initialShootEnergy)
        {
            canFire = true;
        }
    }

	// Use this for initialization
	void Start () {
        lastShootTime = 0.0F;
        lastReloadTime = 0.0F;
        initialShootEnergy = ShootEnergy;
        canFire = true;
        bulletFactory = GameObject.Find("GameManager").GetComponent<BulletFactory>();
    }
	
	// Update is called once per frame
	void Update () {
        if (Time.time - lastReloadTime > reloadCooldown)
        {
            Reload();
            lastReloadTime = Time.time;
        }
	}
}
